package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/sirupsen/logrus"
)

type config struct {
	httpListenAddr string
	esvAPIKey      string
}
type app struct {
	cfg config
	log *logrus.Logger
}

func parseFlags() config {
	cfg := config{}
	flag.StringVar(&cfg.httpListenAddr, "http-listen-addr", ":8000",
		"host:port to listen for HTTP requests")
	flag.Parse()

	cfg.esvAPIKey = os.Getenv("SMSBIBLE_ESV_API_KEY")
	return cfg
}

func (a *app) setupRoutes(handler *http.ServeMux) {
	handler.HandleFunc("/webhook_sms", func(rw http.ResponseWriter, req *http.Request) {
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(rw, "error reading body: %s", err)
			return
		}

		queryStr := url.Values{
			"q":                     []string{string(body)},
			"include-headings":      []string{"0"},
			"include-subheadings":   []string{"0"},
			"include-verse-numbers": []string{"false"},
			"include-footnotes":     []string{"false"},
			"line-length":           []string{"40"},
		}.Encode()

		urlStr := fmt.Sprintf("https://api.esv.org/v3/passage/text/?") + queryStr
		esvReq, err := http.NewRequest("GET", urlStr, nil)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(rw, "error creating request: %s", err)
			return
		}
		esvReq.Header.Set("Authorization", "Token "+a.cfg.esvAPIKey)

		rsp, err := http.DefaultClient.Do(esvReq)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			a.log.Errorf("error calling ESV API: %s", err)
			fmt.Fprintf(rw, "error calling ESV API: %s", err)
			return
		}

		body, err = ioutil.ReadAll(rsp.Body)
		if err != nil {
			rw.WriteHeader(http.StatusBadGateway)
			fmt.Fprintf(rw, "error reading ESV API response: %s", err)
			return
		}
		rsp.Body.Close()

		if rsp.StatusCode != http.StatusOK {
			rw.WriteHeader(rsp.StatusCode)
			a.log.WithFields(logrus.Fields{
				"status": rsp.Status,
				"body":   string(body),
			}).Error("invalid HTTP status")
			fmt.Fprintf(rw, "error calling ESV API: %s", string(body))
			return
		}

		fmt.Fprintf(rw, "%s", string(body))
	})
}

func main() {
	a := app{
		log: logrus.New(),
	}
	a.cfg = parseFlags()
	a.log.SetReportCaller(true)

	handler := http.NewServeMux()
	a.setupRoutes(handler)

	a.log.Infof("listening on %s", a.cfg.httpListenAddr)
	err := http.ListenAndServe(a.cfg.httpListenAddr, handler)
	if err != nil {
		a.log.Errorf("error in http.ListenAndServe: %s", err)
		os.Exit(2)
	}
}
